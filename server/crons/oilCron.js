var oil = require('../routes/oils');
var logger = log4js.getLogger('oilCron.js');
var oilBL = require('../businesslogic/oilsBL');

//Update Real-time Rates
new CronJob('0 4 0 * * *', function () {
// new CronJob('*/15 * * * * *', function () {
    logger.debug("Cron calling updateRealTimeOilPrice: "+new Date());
    mongoose.connection.db.listCollections({name: 'oils'})
      .next(function(err, collinfo) {
          if(err) {
          }
          if (collinfo) {
              logger.debug("oils collection exist");
              oil.getRealTimeOilPrice();
          } else {
              logger.debug("oils collection dosen't exist");    
              oilBL.createOilsCollection();            
          }
      });
  }, function () {
    /* This function is executed when the job stops */
  },
    true,
    'Asia/Calcutta'
);