var currencie = require('../routes/currencies');
var currencieBL = require('../businesslogic/currenciesBL');
var logger = log4js.getLogger('currencyCron.js');

//Update Real-time Rates
new CronJob('0 2 0 * * *', function () {
    currencie.getRealTimeExchangeRates();
    logger.debug("Cron calling getRealTimeExchangeRates: "+new Date());
  }, function () {
    /* This function is executed when the job stops */
  },
    true,
    'Asia/Calcutta'
);

//Add Historical Rates
new CronJob('59 59 23 * * *', function () {
  // new CronJob('*/5 * * * * *', function () {
    logger.debug("Cron calling addHistoricalRates: "+new Date());
    currencie.addHistoricalRates();
  }, function () {
    /* This function is executed when the job stops */
  },
    true,
    'Asia/Calcutta'
);