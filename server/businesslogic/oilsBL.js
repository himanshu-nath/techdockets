var fs = require('fs');
require('../models/oil/statescodes');
require('../models/oil/oils');
var StateCodes = mongoose.model('statecodes');
var Oils = mongoose.model('oils');
var consts = require('../const/const');

var logger = log4js.getLogger('oilBL.js');
let limit = 1000;

module.exports = {
    //Fetching state code list from json and storing into DB
    fetchStateCodes: function (res) {
        let jsonContent = fs.readFileSync('./' + consts.STATECODE_FILE_URL);
        let stateCodeContent = JSON.parse(jsonContent);

        async.mapValues(stateCodeContent, function (value, key, callback) {
            var currencie = new StateCodes({
                name: key,
                code: value,
                status: true
            });
            currencie.save(function (err) {
                if (err) {
                } else {
                    callback();
                }
            });
        }, function (err, asyncResult) {
            if (err)
                logger.debug("Failed to add states code to DB");
            logger.debug("Adding state code to DB");
            res.send({ status: true, message: consts.SUCCESS, info: "State code fetch from json and save to DB" });
        });
    },

    //Create oils collection
    createOilsCollection: function () {
        StateCodes.find({}, function (error, result) {
            if (error) {
                logger.debug("createOilsCollection: error while getting count from statecodes collection");
            }
            else {
                if (result != undefined && result.length > 0) {
                    logger.debug("createOilsCollection: record present in statecodes collection");
                    async.filter(result, function (oilResult, callback) {
                        var oil = new Oils({
                            state: oilResult.name,
                            state_code: oilResult.code,
                            status: true
                        });
                        oil.save(function (err) {
                            if (err) {
                            } else {
                                callback(null, !err)
                            }
                        });
                    }, function (err, results) {
                        if (err)
                            logger.debug("createOilsCollection: oil collection creation failed");
                        logger.debug("createOilsCollection: oil collection created successfully");
                    });

                } else {
                    logger.debug("createOilsCollection: record not present in statecodes collection");
                }
            }
        })
    },

    //Fetch real time oils rate
    fetchRealTimeOilsRates: function (oilresults) {
        let searchDate = new Date();
        async.filter(oilresults, function (oilResult, callback) {
            Oils.aggregate([
                { $unwind: "$records" },
                {
                    $project:
                    {
                        "_id": 1, "date": 1, "state_code": 1,
                        day: { $dayOfMonth: "$records.date" },
                        month: { $month: "$records.date" },
                        year: { $year: "$records.date" }
                    }
                }, {
                    $match: { day: searchDate.getDate(), month: searchDate.getMonth() + 1, year: searchDate.getFullYear(), state_code: oilResult.state_code }
                }
            ], function (err, result) {
                if (err)
                    logger.debug("error in addHistoricalRates: " + err);
                else {
                    if (result.length > 0) {
                        callback(null, !err)
                        logger.debug("fetchRealTimeOilsRates: for current date record already present in collection");
                    } else {
                        request({
                            method: consts.GET,
                            url: consts.OILS_ENDPOINT + '?days=1&state=' + oilResult.state_code,
                            headers: {
                                'Content-Type': 'application/json',
                                'X-Mashape-Key': consts.OILS_TOKEN_VALUE
                            }
                        }, function (error, response, results) {
                            if (results != undefined) {
                                results = JSON.parse(results);
                                if (results.length != 0) {
                                    if (results.hasOwnProperty("STATUS")) {
                                        if (results.STATUS == 401)
                                            logger.debug("fetchRealTimeOilsRates: error status: " + results.STATUS + " message: " + results.RESPONSE);
                                    } else {
                                        logger.debug("fetchRealTimeOilsRates: result got success");
                                        async.filter(results, function (result, nestedCallback) {
                                            Oils.update({ "state_code": oilResult.state_code }, {
                                                $push: {
                                                    records: {
                                                        "date": new Date(),
                                                        "price": result.price,
                                                        "company": result.company,
                                                        "city": result.city,
                                                        "oil_type": result.type,
                                                    }
                                                }
                                            }, { safe: true, upsert: true }, function (err, updateResult) {
                                                if (err) {
                                                    logger.debug("fetchRealTimeOilsRates: error while updating " + err);
                                                } else {
                                                    nestedCallback(null, !err)
                                                }
                                            });

                                        }, function (err, asyncResults) {
                                            callback(null, !err)
                                        });
                                    }
                                } else {
                                    logger.debug("size is zero");
                                    callback(null, !err)
                                }
                            }
                        });
                    }
                }
            });
        }, function (err, asyncResults) {
            logger.debug("fetchRealTimeOilsRates: all record added succesfully");
        });
    }
}