var XLSX = require('xlsx');
var ObjectID = require('mongodb').ObjectID;
require('../models/postoffice/postoffices');
var Postoffices = mongoose.model('postoffices');
var consts = require('../const/const');

var logger = log4js.getLogger('postofficesBL.js');
let limit = 1000;

module.exports = {
    //Fetching postoffice list from excel sheet and storing into DB
    addPostofficeDataToDB: function (res) {        
        var postofficeWorkbook = XLSX.readFile('./' + consts.POSTOFFICE_FILE_URL);
        var postoffice_name_list = postofficeWorkbook.SheetNames;
        var postOffices = XLSX.utils.sheet_to_json(postofficeWorkbook.Sheets[postoffice_name_list[0]]);
        
        postOffices = postOffices.map(function (el) {
            var o = Object.assign({}, el);
            o.status = true;
            o.cot = new Date();
            o.mot = new Date();
            return o;
        })

        console.log(postOffices.length);
        async.series([
            function (callback) {
                while (postOffices.length) {
                    let limitRecord = postOffices.splice(0, limit);
                    logger.debug('Postoffice record length: ' + postOffices.length);
                    logger.debug('Postoffice Slice record length: ' + limitRecord.length);
                    async.series([
                        function (callback) {
                            conn.collection('postoffices').insertMany(limitRecord)
                                .then(function (mongooseDocuments) {
                                    callback();
                                })
                                .catch(function (err) {
                                    logger.debug('Error while insertMany of uploadPostoffice: ' + err);
                                });
                        }
                    ],
                        function (err, results) {
                            if (err) {
                                logger.debug('Error while nested series of uploadPostoffice: ' + err);
                            }
                        });
                }
                callback();
            }
        ],
            function (err, results) {
                if (err) {
                    logger.debug('Error while calling first series ' + err);
                }
                logger.debug('Record inserted succesfully of uploadPostoffice');
                res.send({ status: true, message: consts.SUCCESS, developerInfo: 'Record inserted succesfully of uploadPostoffice' });                
            });
    }
}