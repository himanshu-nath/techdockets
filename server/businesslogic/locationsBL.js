require('../models/location/countries');
var Countries = mongoose.model('countries');
var consts = require('../const/const');
const csv=require('csvtojson');

var logger = log4js.getLogger('locationsBL.js');

module.exports = {
    //Upload Countries
    uploadCountries: function (res) {        
        csv()
        .fromFile('./' + consts.COUNTRIES_CSV_URL)
        .on('json',(countryObj)=>{
            var country = new Countries({
                countryName : countryObj['Country Name'],
                iso2 : countryObj.ISO2,
                iso3 : countryObj.ISO3,
                topLevelDomain : countryObj['Top Level Domain'],
                fips : countryObj.FIPS,
                isoNumeric : countryObj['ISO Numeric'],
                geoNameId : countryObj.GeoNameID,
                e164 : countryObj.E164,
                phoneCode : countryObj['Phone Code'],
                continent : countryObj.Continent,
                capital : countryObj.Capital,
                timeZone : countryObj['Time Zone'],
                currency : countryObj.Currency,
                languageCodes : countryObj['Language Codes'],
                languages : countryObj.Languages,
                areaKM2 : countryObj['Area KM2'],
                internetHosts : countryObj['Internet Hosts'],
                internetUsers : countryObj['Internet Users'],
                mobile : countryObj['Phones (Mobile)'],
                landline : countryObj['Phones (Landline)'],
                gdp : countryObj.GDP,
                stauts : true
            });
            country.save(function (err) {
                if (err) {
                } else {
                }
            });
        })
        .on('done',(error)=>{
            if(error) {
                logger.debug('UploadCountries: error while importing country list to DB');   
                res.send({ status: false, message: consts.ERROR, error});
            } 
            logger.debug('UploadCountries: countries importing to DB completed');
            res.send({ status: true, message: consts.SUCCESS, developerMsg: 'Countries record uploaded successfully'});
        })
    },

    //Location finder
    locationFinder: function (req, res) {        
        request({
            method: consts.GET,
            url: consts.LOCATION_ENDPOINT + 'json?components=locality:' + req.body.address + '|country:' 
            + req.body.countryIso2 + '&key=' + consts.LOCATION_TOKEN,
            headers: { 'Content-Type': 'application/json' }
        }, function (error, response, results) {
            if(error)
            res.send({ status: false, message: consts.ERROR, error });
            if (results != undefined) {
                results = JSON.parse(results);
                if (results.length != 0) {
                    logger.debug("locationFinder: successfully find the location");
                    res.send({ status: true, message: consts.SUCCESS, results });
                } else {
                    logger.debug("locationFinder: result is undefined");
                    res.send({ status: true, message: consts.FAIL });
                }
            }
        });
    }
}