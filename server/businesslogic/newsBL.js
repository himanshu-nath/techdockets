var consts = require('../const/const');
var logger = log4js.getLogger('newsBL.js');

module.exports = {
    //Top headline
    topHeadline: function (req, res) {
        let url;
        if (req.query.search !== "") {
            url = consts.GOOGLE_NEWS_URL_ENDPOINT + 'top-headlines?' + 'q=' + req.query.search;
        } else if (req.query.source !== "") {
            url = consts.GOOGLE_NEWS_URL_ENDPOINT + 'top-headlines?' + 'sources=' + req.query.source;
        } else {
            if (req.query.countryCode === "") {
                res.send({ status: false, message: consts.ERROR, developerMsg: "Country Code can't be null" });
                return;
            }
            let category = req.query.category !== "" ? req.query.category : "";
            url = consts.GOOGLE_NEWS_URL_ENDPOINT + 'top-headlines?' + 'country=' + req.query.countryCode + '&category=' + category;
        }
        console.log(url)
        request({
            method: consts.GET,
            url: url,
            headers: { 'Content-Type': 'application/json', 'X-Api-Key': consts.GOOGLE_NEWS_API_KEY }
        }, function (error, response, results) {
            if (error)
                res.send({ status: false, message: consts.ERROR, error });
            if (results != undefined) {
                results = JSON.parse(results);
                if (results.length != 0) {
                    logger.debug("topHeadline: successfully get the result");
                    res.send({ status: true, message: consts.SUCCESS, results });
                } else {
                    logger.debug("topHeadline: result is undefined");
                    res.send({ status: true, message: consts.FAIL });
                }
            }
        });
    },

    //Everything
    everything: function (req, res) {
        let url;
        if (req.query.search != undefined) {
            let search = req.query.search != undefined ? req.query.search : "";
            let source = req.query.source != undefined ? req.query.source : "";
            let sortBy = req.query.sortBy != undefined ? req.query.sortBy : "";
            let language = req.query.language != undefined ? req.query.language : "";
            let domain = req.query.domains != undefined ? req.query.domains : "";
            let pageNumber = req.query.pageNumber != undefined ? req.query.pageNumber : 1;
            let pageOffset = req.query.pageOffset != undefined ? req.query.pageOffset : 40;
            url = consts.GOOGLE_NEWS_URL_ENDPOINT + 'everything?' + 'q=' + search + '&sources=' + source +
                '&sortBy=' + sortBy + '&language=' + language + '&domains=' + domain +
                '&page=' + pageNumber + '&pageSize=' + pageOffset;
            console.log(url)
            request({
                method: consts.GET,
                url: url,
                headers: { 'Content-Type': 'application/json', 'X-Api-Key': consts.GOOGLE_NEWS_API_KEY }
            }, function (error, response, results) {
                if (error)
                    res.send({ status: false, message: consts.ERROR, error });
                if (results != undefined) {
                    results = JSON.parse(results);
                    if (results.length != 0) {
                        logger.debug("everything: successfully get the result");
                        res.send({ status: true, message: consts.SUCCESS, results });
                    } else {
                        logger.debug("everything: result is undefined");
                        res.send({ status: true, message: consts.FAIL });
                    }
                }
            });
        } else {
            res.send({ status: false, message: consts.ERROR, developerMsg: "Search filter is required" });
        }
    },

    //Get source
    getSources: function (req, res) {
        let url;
        let language = req.query.language != undefined ? req.query.language : "";
        let category = req.query.category != null ? req.query.category : "";
        let countryCode = req.query.countryCode != null ? req.query.countryCode : "";
        url = consts.GOOGLE_NEWS_URL_ENDPOINT + 'sources?' + '&language=' + language + '&category=' +
            category + '&countryCode=' + countryCode;
        console.log(url)
        request({
            method: consts.GET,
            url: url,
            headers: { 'Content-Type': 'application/json', 'X-Api-Key': consts.GOOGLE_NEWS_API_KEY }
        }, function (error, response, results) {
            if (error)
                res.send({ status: false, message: consts.ERROR, error });
            if (results != undefined) {
                results = JSON.parse(results);
                if (results.length != 0) {
                    logger.debug("getSources: successfully get the result");
                    res.send({ status: true, message: consts.SUCCESS, results });
                } else {
                    logger.debug("getSources: result is undefined");
                    res.send({ status: true, message: consts.FAIL });
                }
            }
        });
    }
}