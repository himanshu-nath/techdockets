var consts = require('../const/const');

var logger = log4js.getLogger('urlsBL.js');
let limit = 1000;

module.exports = {
    //URL shortner
    urlShortener: function (req, res) {        
        request({
            method: consts.POST,
            url: consts.URL_ENDPOINT + 'url?key=' + consts.URL_TOKEN,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ longUrl: req.body.url })
        }, function (error, response, results) {
            if(error)
            res.send({ status: false, message: consts.ERROR, error });
            if (results != undefined) {
                results = JSON.parse(results);
                if (results.length != 0) {
                    logger.debug("urlShortener: successfully compress the url");
                    res.send({ status: true, message: consts.SUCCESS, results });
                } else {
                    logger.debug("urlShortener: result is undefined");
                    res.send({ status: true, message: consts.FAIL });
                }
            }
        });
    },

    //URL shortner
    urlExpand: function (req, res) {        
        request({
            method: consts.GET,
            url: consts.URL_ENDPOINT + 'url?key=' + consts.URL_TOKEN + '&shortUrl=' + req.body.url,
            headers: { 'Content-Type': 'application/json' }
        }, function (error, response, results) {
            if(error)
            res.send({ status: false, message: consts.ERROR, error });
            if (results != undefined) {
                results = JSON.parse(results);
                if (results.length != 0) {
                    logger.debug("urlShortener: successfully compress the url");
                    res.send({ status: true, message: consts.SUCCESS, results });
                } else {
                    logger.debug("urlShortener: result is undefined");
                    res.send({ status: true, message: consts.FAIL });
                }
            }
        });
    }
}