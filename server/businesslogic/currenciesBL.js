// var async = require('async');
require('../models/currencies');
require('../models/currencyrates');
require('../models/currencyhistorical');
var Currencies = mongoose.model('currencie');
var CurrencyRates = mongoose.model('currencyrate');
var CurrencyHistories = mongoose.model('currencyhistories');
var consts = require('../const/const');

var logger = log4js.getLogger('currenciesBL.js');

module.exports = {
    //Fetching currency list and storing into DB
    fetchCurrencyCodeList: function (res) {
        request({
            method: consts.GET,
            url: consts.CURRENCY_ENDPOINT + 'list?access_key=' + consts.CURRENCY_ACCESS_KEY,
        }, function (error, response, result) {
            result = JSON.parse(result);
            if (result.hasOwnProperty("error")) {
                if (result.error.code == 104)
                    res.status(104).send({ status: result.success, message: result.error.info });
            } else {
                logger.debug("Calling  currenciesBL : fetchCurrencyList method ");
                async.mapValues(result.currencies, function (value, key, callback) {
                    var currencie = new Currencies({
                        name: value,
                        code: key,
                        status: true
                    });
                    currencie.save(function (err) {
                        if (err) {
                        } else {
                            callback();
                        }
                    });
                }, function (err, asyncResult) {
                    logger.debug("Adding currencies to DB");
                    res.send({ status: result.success, message: consts.SUCCESS, info: "Currencies fetch from apilayer.net and save to DB" });
                });
            }
        })
    },

    //Fetching real time exchange rate from apilayer.net
    fetchExchangeRates: function () {
        request({
            method: consts.GET,
            url: consts.CURRENCY_ENDPOINT + 'live?access_key=' + consts.CURRENCY_ACCESS_KEY,
        }, function (error, response, result) {
            result = JSON.parse(result);
            if (result.hasOwnProperty("error")) {
                if (result.error.code == 104)
                    logger.debug("Failed to fetch currency rates: " + result.error.info);
            } else {
                logger.debug("Calling  currenciesBL : fetchCurrencyList method ");
                async.mapValues(result.quotes, function (value, key, callback) {
                    var currencyrate = new CurrencyRates({
                        code: key.slice(3),
                        rate: value,
                        source: result.source
                    });
                    currencyrate.save(function (err) {
                        if (err) {
                        } else {
                            callback();
                        }
                    });
                }, function (err, asyncResult) {
                    logger.debug("Adding currency rates to DB");                
                });
            }
        })
    },

    //Updating real time exchange rate from apilayer.net
    updateExchangeRates: function () {
        request({
            method: consts.GET,
            url: consts.CURRENCY_ENDPOINT + 'live?access_key=' + consts.CURRENCY_ACCESS_KEY,
        }, function (error, response, result) {
            result = JSON.parse(result);
            if (result.hasOwnProperty("error")) {
                if (result.error.code == 104)
                    logger.debug("Failed to fetch currency rates: " + result.error.info);
            } else {
                logger.debug("Calling  currenciesBL : updateExchangeRates method ");
                async.mapValues(result.quotes, function (value, key, callback) {
                    CurrencyRates.update({code : key},  { $set: { rate: value, date: new Date(),
                        mot: new Date() }}, { multi: true }, function(err, updateResult) {
                        if (err){
                            logger.debug("Failed to update exchange rate for code: "+key);
                        } else {
                            callback();
                        }        
                    });
                }, function (err, asyncResult) {
                    logger.debug("Updated currency rates to DB");                  
                });
            }
        })
    },

    //Adding real time exchange rate to currencyhistories collection
    addingHistoricalRates: function () {
        CurrencyRates.find({}, function (err, exchangeRateResults) {
            if (err) {
                logger.debug("Error occur while fetching CurrencyRates data");
            }
            else {
                let currencyList = [];
                async.map(exchangeRateResults, function(exchangeRateResult, callback) {
                    let currency = {}
                    currency.code = exchangeRateResult.code;
                    currency.rate = exchangeRateResult.rate;
                    currencyList.push(currency);
                    callback();
                }, function(err, results) {
                    if( err ) {
                        logger.debug('Error while async in addingHistoricalRates method');                    
                    } else {
                        var currencyHistory = new CurrencyHistories({
                            source: exchangeRateResults.source,
                            date: new Date(),
                            currencies: currencyList
                        });
                        currencyHistory.save(function (err) {
                            if (err) {
                            } else {
                                logger.debug('Successfully added CurrencyRates in currencyhistories collection');
                            }
                        });
                    }
                });
            }
        });
    },

    //Fetching exchange rate and storing in DB
    fetchHistoricalRates: function (searchDate, res) {
        var dateformat = function (i) { return (i < 10 ? '0' : '') + i };
        request({
            method: consts.GET,
            url: consts.CURRENCY_ENDPOINT + 'historical?access_key=' + consts.CURRENCY_ACCESS_KEY + "&date="+searchDate.getFullYear()+"-"+tf(searchDate.getMonth()+1)+"-"+tf(searchDate.getDate())
        }, function (error, response, result) {
            result = JSON.parse(result);
            if (result.hasOwnProperty("error")) {
                if (result.error.code == 104)
                    logger.debug("Failed to fetch historical currency rates: " + result.error.info);
                else if (result.error.code == 302)
                    logger.debug("Failed to fetch historical currency rates: " + result.error.info);
            } else {
                logger.debug("Calling  currenciesBL : fetchHistoricalRates method ");
                let currencyList = [];
                async.mapValues(result.quotes, function (value, key, callback) {
                    let currency = {}
                    currency.code = key;
                    currency.rate = value;
                    currencyList.push(currency);
                    callback();
                }, function (err, asyncResult) {
                    if( err ) {
                        logger.debug('Error while async in fetchHistoricalRates method');                    
                    } else {
                        var currencyHistory = new CurrencyHistories({
                            source: result.source,
                            date: searchDate,
                            currencies: currencyList
                        });
                        currencyHistory.save(function (err, result) {
                            if (err) {
                            } else {
                                res.send({ status: true, message: consts.SUCCESS, result });
                                logger.debug('fetchHistoricalRates : Successfully added CurrencyRates in currencyhistories collection');
                            }
                        });
                    }              
                });
            }
        });
    }
}