require('../models/translate/languages');
var Languages = mongoose.model('languages');
var consts = require('../const/const');

var logger = log4js.getLogger('translatesBL.js');
let limit = 1000;

var Sync = require('sync');

var TextToSpeechV1 = require('watson-developer-cloud/text-to-speech/v1');
var SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
var fs = require('fs');
var mkdirp = require('mkdirp');

module.exports = {
    //Uploading languages to DB
    addLanguageDataToDB: function (res) {
        request({
            method: consts.POST,
            url: consts.LANGUAGE_ENDPOINT + 'getLangs?ui=en&key=' + consts.LANGUAGE_TOKEN_VALUE,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, results) {
            if (results != undefined) {
                results = JSON.parse(results);
                if (results.length != 0) {
                    logger.debug("addLanguageDataToDB: result got success");
                    async.forEachOf(results.langs, function (value, key, callback) {
                        var language = new Languages({
                            name: value,
                            code: key,
                            status: true
                        });
                        language.save(function (err) {
                            if (err) {
                            } else {
                                callback();
                            }
                        });
                    }, function (err, asyncResults) {
                        if (err) logger.debug("addLanguageDataToDB: failed to add: " + err);
                        logger.debug("addLanguageDataToDB: added successfully");
                        res.send({ status: true, message: consts.SUCCESS, count: results.langs.length });
                    });
                } else {
                    logger.debug("addLanguageDataToDB: size is zero");
                }
            }
        });
    },

    //Detect languages
    languageDetect: function (req, res) {
        request({
            method: consts.GET,
            url: consts.LANGUAGE_ENDPOINT + 'detect?key=' + consts.LANGUAGE_TOKEN_VALUE + '&text=' + req.query.text,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, results) {
            if (results != undefined) {
                results = JSON.parse(results);
                if (results.code == 200) {
                    logger.debug("languageDetect: language got detected successfully");
                    Languages.findOne({ code: results.lang }, function (err, result) {
                        if (err) {
                            res.send({ status: false, languageCode: results.lang, message: "error occurred while fetching code in DB", err });
                        }
                        else {
                            logger.debug("languageDetect: detect is present in languages collection");
                            res.send({ status: true, message: consts.SUCCESS, result });
                        }
                    });
                } else {
                    logger.debug("languageDetect: language detected got failed");
                }
            }
        });
    },

    //Convert languages
    languageConvert: function (req, res) {
        request({
            method: consts.GET,
            url: consts.LANGUAGE_ENDPOINT + 'translate?lang=' + req.body.from + '-' + req.body.to +
                '&key=' + consts.LANGUAGE_TOKEN_VALUE + '&text=' + req.body.text,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, results) {
            if (results != undefined) {
                results = JSON.parse(results);
                console.log(results)
                if (results.code == 200) {
                    logger.debug("languageConvert: language got detected successfully");
                    res.send({ status: true, message: consts.SUCCESS, language: results.lang, text: results.text });
                } else {
                    logger.debug("languageConvert: language detected got failed");
                    res.send({ status: true, message: consts.FAIL, results });
                }
            }
        });
    }

}