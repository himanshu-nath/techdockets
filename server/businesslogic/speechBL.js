var consts = require('../const/const');

var logger = log4js.getLogger('speechBL.js');

var Sync = require('sync');
var TextToSpeechV1 = require('watson-developer-cloud/text-to-speech/v1');
var SpeechToTextV1 = require('watson-developer-cloud/speech-to-text/v1');
var fs = require('fs');
var mkdirp = require('mkdirp');

module.exports = {
    //Text to speech conversion
    textToSpeech: function (req, res) {
        var textToSpeech = new TextToSpeechV1({
            username: consts.WATSON_DEVELOPER_CLOUD_TEXT_TO_SPEECH_USERNAME,
            password: consts.WATSON_DEVELOPER_CLOUD_TEXT_TO_SPEECH_PASSWORD,
            url: consts.TEXT_TO_SPEECH_URL,
            headers: {
                'X-Watson-Learning-Opt-Out': 'true'
            }
        });

        var params = {
            text: req.query.text,
            voice: req.query.voice, // Optional voice
            accept: 'audio/wav'
            //accept: 'audio/mp3'
            //voice: 'en-US_AllisonVoice'
        };
        let filename = __dirname + '/../../' + consts.TEXT_TO_SPEECH_URL_DIR + 'audio-' + new Date().getTime() + '.wav';

        // Synthesize speech, correct the wav header, then save to disk
        // (wav header requires a file length, but this is unknown until after the header is already generated and sent)
        textToSpeech
            .synthesize(params, function (err, audio) {
                if (err) {
                    if (err) logger.debug("textToSpeech: failed to convert text to speech due to: " + err);
                    res.send({ status: false, message: consts.FAIL, err });
                }
                textToSpeech.repairWavHeader(audio);
                if (fs.existsSync(__dirname + '/../../' + consts.TEXT_TO_SPEECH_URL_DIR)) {
                    logger.debug("textToSpeech: Hurray directory exists and audio file saved");
                    fs.writeFileSync(filename, audio);
                    getAudioStrem(filename, req, res);
                } else {
                    mkdirp(__dirname + '/../../' + consts.TEXT_TO_SPEECH_URL_DIR, function (err) {
                        if (err) logger.debug("textToSpeech: Oops folder creation failed");
                        else {
                            logger.debug("textToSpeech: Hurray folder created successfully and audio filed saved");
                            fs.writeFileSync(filename, audio);
                            getAudioStrem(filename, req, res);
                        }
                    });
                }
                //res.send({ status: true, message: consts.SUCCESS });
            });     
    },

    //Speech languags list
    speechLanguages: function (req, res) {
        var textToSpeech = new TextToSpeechV1({
            username: consts.WATSON_DEVELOPER_CLOUD_TEXT_TO_SPEECH_USERNAME,
            password: consts.WATSON_DEVELOPER_CLOUD_TEXT_TO_SPEECH_PASSWORD,
            url: consts.TEXT_TO_SPEECH_URL,
            headers: {
                'X-Watson-Learning-Opt-Out': 'true'
            }
        });

        var params = {
            text: req.body.text,
            voice: req.body.voice,
            accept: 'audio/wav'
            //voice: 'en-US_AllisonVoice',
            //accept: 'audio/mp3'
        };

        // Synthesize speech, correct the wav header, then save to disk
        // (wav header requires a file length, but this is unknown until after the header is already generated and sent)
        textToSpeech.listVoices(null, function (error, voices) {
            if (error) {
                logger.debug("speechLanguages: Oops to get languages list: " + error);
                res.send({ status: false, message: consts.FAIL, error });
            } else {
                logger.debug("speechLanguages: Hurray got languages list");
                res.send({ status: true, message: consts.SUCCESS, languages: voices.voices });
            }
        });
    },

    //Speech to text conversion
    speechToText: function (req, res) {
        let filename = __dirname + '/../../' + consts.SPEECH_TO_TEXT_URL_DIR + 'speech-' + new Date().getTime() + '.wav';
        if (!req.files)
            return res.status(400).send({ message: "No files were uploaded" });
        let speechFile = req.files.audioFile;
        Sync(function () {
            function checkDirectory(callback) {
                if (fs.existsSync(__dirname + '/../../' + consts.SPEECH_TO_TEXT_URL_DIR)) {
                    logger.debug("speechToText: Hurray directory exists and audio file saved");
                    speechFile.mv(filename, function (err) {
                        if (err) return res.status(500).send(err);
                        //res.send('File uploaded!');
                        callback(null);
                    });
                } else {
                    mkdirp(__dirname + '/../../' + consts.SPEECH_TO_TEXT_URL_DIR, function (err) {
                        if (err) logger.debug("speechToText: Oops folder creation failed");
                        else {
                            logger.debug("speechToText: Hurray folder created successfully and audio filed saved");
                            // fs.writeFileSync(__dirname + '/../../' + consts.SPEECH_TO_TEXT_URL_DIR + 'audio-' + new Date().getTime() + '.wav', audio);
                            speechFile.mv(filename, function (err) {
                                if (err) return res.status(500).send(err);
                                //res.send('File uploaded!');
                                callback(null);
                            });
                        }
                    });
                }
            }

            checkDirectory.sync(null);
            console.log(filename);
            var speech_to_text = new SpeechToTextV1({
                username: consts.WATSON_DEVELOPER_CLOUD_SPEECH_TO_TEXT_USERNAME,
                password: consts.WATSON_DEVELOPER_CLOUD_SPEECH_TO_TEXT_PASSWORD
            });
            var params = {
                audio: fs.createReadStream(filename),
                content_type: 'audio/wav',
                interim_results: false,
                max_alternatives: 1,
                timestamps: false,
                word_alternatives_threshold: 0.9,
                keywords: ['colorado', 'tornado', 'tornadoes'],
                keywords_threshold: 0.5
            };

            speech_to_text.recognize(params, function (error, transcript) {
                if (error) console.log('Error:', error);
                else {
                    // console.log(JSON.stringify(transcript, null, 2));
                    res.send({
                        transcript: transcript.results[0].alternatives[0].transcript, confidence:
                            transcript.results[0].alternatives[0].confidence
                    });
                }
            });
        })

    }
}

function getAudioStrem(filename, req, res) {
    console.log(filename);
    try {
        var stat = fs.statSync(filename);
        var total = stat.size;

        if (req.headers.range) {
            var range = req.headers.range;
            var parts = range.replace(/bytes=/, "").split("-");
            var partialstart = parts[0];
            var partialend = parts[1];

            var start = parseInt(partialstart, 10);
            var end = partialend ? parseInt(partialend, 10) : total - 1;
            var chunksize = (end - start) + 1;
            console.log('RANGE: ' + start + ' - ' + end + ' = ' + chunksize);

            var file = fs.createReadStream(filename, { start: start, end: end });
            res.writeHead(206, { 'Content-Range': 'bytes ' + start + '-' + end + '/' + total, 'Accept-Ranges': 'bytes', 'Content-Length': chunksize, 'Content-Type': 'audio/wav'});
            file.pipe(res);

        } else {
            console.log('ALL: ' + total);
            res.writeHead(200, { 'Content-Length': total, 'Content-Type': 'audio/wav' });
            fs.createReadStream(filename).pipe(res);
        }
    } catch (err) {
        console.log('Error:', err);
        res.status(500).send({ status: false, message: "File Not Found!", err });
    }
}