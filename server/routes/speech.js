var consts = require('../const/const');
var speechBL = require('../businesslogic/speechBL');

var logger = log4js.getLogger('speech.js');

module.exports = {
    //Text To Speech conversion
    textToSpeech: function (req, res) {
        speechBL.textToSpeech(req, res);        
    },
    
    //Speech To Text conversion
    speechToText: function (req, res) {
        speechBL.speechToText(req, res);
        //TODO have to create new method
    },

    //Speech languages list
    speechLanguageList: function (req, res) {
        speechBL.speechLanguages(req, res);
    }
};