var consts = require('../const/const');
var urlBL = require('../businesslogic/urlsBL');

var logger = log4js.getLogger('urls.js');

module.exports = {
    //URL shortener
    shortener: function (req, res) {
        urlBL.urlShortener(req, res);        
    },

    //URL expand
    expand: function (req, res) {
        urlBL.urlExpand(req, res);        
    }
};