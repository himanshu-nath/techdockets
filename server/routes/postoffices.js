// var async = require('async');
require('../models/postoffice/postoffices');
var Postoffices = mongoose.model('postoffices');
var consts = require('../const/const');
var postofficeBL = require('../businesslogic/postofficesBL');

var logger = log4js.getLogger('postoffices.js');

module.exports = {
    //Upload postoffice
    uploadPostoffice: function (req, res) {
        Postoffices.count({}, function (err, result) {
            if (err) {
                res.send({ status: false, message: "error occurred while getting count of record in postoffices", err });
            }
            else {
                if (result != undefined && result > 0) {
                    logger.debug("uploadPostoffice: record present in postoffices collection");
                    res.send({ status: true, message: consts.SUCCESS, count: result });
                } else {
                    logger.debug("uploadPostoffice: data is fetching from ecxel sheet and will store into postoffices collection");
                    postofficeBL.addPostofficeDataToDB(res)
                }
            }
        });
    },

    //Get postoffice list (with Pagination)
    getPostofficeList: function (req, res) {
        let page = Number(req.query.page) || Number(req.params.page)
        let limit = Number(req.query.limit) || Number(req.params.limit)
        
        Postoffices.paginate({}, { page: page, limit: limit })
        .then(function (result) {
            res.send({ status: true, message: consts.SUCCESS, result });
            logger.debug('getPostofficeList: result found successfully');
        })
        .catch(function (err) {
            res.send({ status: false, message: "error occurred while getting list of postoffices in getPostofficeList", err });
            logger.debug('getPostofficeList: Error while fetching record from postoffice collection: ' + err);
        });
    },

    //Get postoffice by pincode
    getPostofficeByPincode: function (req, res) {      
        Postoffices.paginate({pincode: req.params.pincode}, { page: Number(req.params.page), limit: Number(req.params.limit) })
        .then(function (result) {
            res.send({ status: true, message: consts.SUCCESS, result });
            logger.debug('getPostofficeByPincode: result found successfully');
        })
        .catch(function (err) {
            res.send({ status: false, message: "error occurred while getting list of postoffices in getPostofficeList", err });
            logger.debug('getPostofficeByPincode: Error while fetching record by pincode number: ' + err);
        });
    },

    //Get postoffice by state name (with Pagination)
    getPostofficeByState: function (req, res) {
        Postoffices.paginate({ state_name: new RegExp('^'+req.params.state+'$', "i") }, { page: Number(req.params.page), limit: Number(req.params.limit) })
        .then(function (result) {
            res.send({ status: true, message: consts.SUCCESS, result });
            logger.debug('getPostofficeByState: result found successfully');
        })
        .catch(function (err) {
            res.send({ status: false, message: "error occurred while getting list of postoffices in getPostofficeList", err });
            logger.debug('getPostofficeByState: Error while fetching record by state name: ' + err);
        });
    },

    //Get postoffice by district name (with Pagination)
    getPostofficeByDistrict: function (req, res) {
        Postoffices.paginate({ district_name: new RegExp('^'+req.params.district+'$', "i") }, { page: Number(req.params.page), limit: Number(req.params.limit) })
        .then(function (result) {
            res.send({ status: true, message: consts.SUCCESS, result });
            logger.debug('getPostofficeByDistrict: result found successfully');
        })
        .catch(function (err) {
            res.send({ status: false, message: "error occurred while getting list of postoffices in getPostofficeList", err });
            logger.debug('getPostofficeByDistrict: Error while fetching record by district name: ' + err);
        });
    },

    //Get postoffice by city name (with Pagination)
    getPostofficeByCity: function (req, res) {
        Postoffices.paginate({ taluk: new RegExp('^'+req.params.city+'$', "i") }, { page: Number(req.params.page), limit: Number(req.params.limit) })
        .then(function (result) {
            res.send({ status: true, message: consts.SUCCESS, result });
            logger.debug('getPostofficeByCity: result found successfully');
        })
        .catch(function (err) {
            res.send({ status: false, message: "error occurred while getting list of postoffices in getPostofficeList", err });
            logger.debug('getPostofficeByCity: Error while fetching record by city name: ' + err);
        });
    },

    //Get postoffice by office name (with Pagination)
    getPostofficeByOffice: function (req, res) {
        Postoffices.paginate({ office_name: new RegExp('.*'+req.params.office+'.*', "i") }, { page: Number(req.params.page), limit: Number(req.params.limit) })
        .then(function (result) {
            res.send({ status: true, message: consts.SUCCESS, result });
            logger.debug('getPostofficeByOffice: result found successfully');
        })
        .catch(function (err) {
            res.send({ status: false, message: "error occurred while getting list of postoffices in getPostofficeList", err });
            logger.debug('getPostofficeByOffice: Error while fetching record by office name: ' + err);
        });
    }
};