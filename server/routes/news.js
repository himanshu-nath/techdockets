var consts = require('../const/const');
var newsBL = require('../businesslogic/newsBL');

var logger = log4js.getLogger('news.js');

module.exports = {
    //Top headlines
    topHeadline: function (req, res) {
        newsBL.topHeadline(req, res);        
    },

    //Everything
    everything: function (req, res) {
        newsBL.everything(req, res);        
    },

    //Get Source
    getSources: function (req, res) {
        newsBL.getSources(req, res);        
    }
};