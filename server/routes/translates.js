require('../models/translate/languages');
var Languages = mongoose.model('languages');
var consts = require('../const/const');
var languageBL = require('../businesslogic/translatesBL');

var logger = log4js.getLogger('translates.js');

module.exports = {
    //Upload languages
    uploadLanguages: function (req, res) {
        Languages.count({}, function (err, result) {
            if (err) {
                res.send({ status: false, message: "error occurred while getting count of record in languages", err });
            }
            else {
                if (result != undefined && result > 0) {
                    logger.debug("uploadLanguages: record present in languages collection");
                    res.send({ status: true, message: consts.SUCCESS, count: result });
                } else {
                    logger.debug("uploadLanguages: data uploading is started");
                    languageBL.addLanguageDataToDB(res)
                }
            }
        });
    },

    //Language List
    languageList: function (req, res) {
        Languages.find({}, null, {sort: {name: 1}}, function (err, result) {
            if (err) {
                res.send({ status: false, message: "error occurred while getting list of record in languages", err });
            } else {
                if (result != undefined && result.length > 0) {
                    logger.debug("languageList: record found in DB: "+result.length);
                    res.send({ status: true, message: consts.SUCCESS, count: result.length, result });
                } else {
                    logger.debug("languageList: 0 record found in DB");
                    res.send({ status: true, message: consts.SUCCESS, count: 0 });
                }
            }
        });
    },

    //Detech Language
    languageDetect: function (req, res) {
        languageBL.languageDetect(req, res);        
    },

    //Detech Language
    languageConvert: function (req, res) {
        languageBL.languageConvert(req, res);        
    }
};