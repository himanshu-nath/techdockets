require('../models/oil/statescodes');
require('../models/oil/oils');
var StateCodes = mongoose.model('statecodes');
var Oils = mongoose.model('oils');
var consts = require('../const/const');
var oilsBL = require('../businesslogic/oilsBL');
var oilCron = require('../crons/oilCron.js');

var logger = log4js.getLogger('oils.js');

module.exports = {
    //Upload state code currencies list
    getStateCodes: function (req, res) {
        StateCodes.find({}, function (err, result) {
            if (err) {
                res.send({ status: false, message: "error occurred", err });
            }
            else {
                if (result.length > 0) {
                    logger.debug("getStateCodes: record present in statecode collection");
                    res.send({ status: true, message: consts.SUCCESS, result });
                } else {
                    logger.debug("getStateCodes: record not present in collection now fetching from json");
                    oilsBL.fetchStateCodes(res);
                }
            }
        });
    },

    //Fetch real time price from third party
    getRealTimeOilPrice: function (req, res) {
        Oils.find({}, function (err, result) {
            if (err) {
                logger.debug("getRealTimeOilPrice: error while getting count from oils collection");
            }
            else {
                if(result.length == 0)
                    oilsBL.createOilsCollection();
                logger.debug("getRealTimeOilPrice: record present in oils collection");
                oilsBL.fetchRealTimeOilsRates(result);
            }
        });
    },

    //get oil price by state
    getOilInfoByState: function (req, res) {
        Oils.findOne({ state_code: req.params.state }, function (err, result) {
            if (err) {
                logger.debug("getOilInfoByState: error while getting records from oils collection");
            }
            else {
                logger.debug("getRealTimeOilPrice: record present in oils collection");
                res.send({ status: true, message: consts.SUCCESS, result });
            }
        });
    },

    //get oil info by filtering
    getOilInfoByFiltering: function (req, res) {
        Oils.aggregate([
            { $unwind: "$records" },
            {
                $match: {
                    "records.city": { $regex: new RegExp(req.query.city, "i") },
                    "records.company": { $regex: new RegExp(req.query.company, "i") },
                    "records.oil_type": { $regex: new RegExp(req.query.oilType, "i") },
                    "records.date": {$gte: new Date(req.query.date), $lt: moment(req.query.date).add(1, 'days').add(18, 'hours').toDate()}
                }
            },
            {
                $group:
                {
                    _id: "$_id",
                    state: { $first: "$state" },
                    state_code: { $first: "$state_code" },
                    records: {
                        $push: {
                            id: "$records._id",
                            city: "$records.city",
                            company: "$records.company",
                            price: "$records.price",
                            date: "$records.date",
                            oilType: "$records.oil_type"
                        }
                    }
                }
            }
        ], function (err, result) {
            if (err)
                logger.debug("error in addHistoricalRates: " + err);
            else {
                res.send({ status: true, message: consts.SUCCESS, result });
            }
        });
    },

    //get oil info by start date and end date
    getOilInfoByDate: function (req, res) {
        Oils.aggregate([
            { $unwind: "$records" },
            {
                $match: {
                    "records.date": {$gte: new Date(req.query.startDate), $lt: new Date(req.query.endDate)}
                }
            },
            {
                $group:
                {
                    _id: "$_id",
                    state: { $first: "$state" },
                    state_code: { $first: "$state_code" },
                    records: {
                        $push: {
                            id: "$records._id",
                            city: "$records.city",
                            company: "$records.company",
                            price: "$records.price",
                            date: "$records.date",
                            oilType: "$records.oil_type"
                        }
                    }
                }
            }
        ], function (err, result) {
            if (err)
                logger.debug("getOilInfoByDate: error " + err);
            else {
                logger.debug("getOilInfoByDate: result got successfully ");
                res.send({ status: true, message: consts.SUCCESS, result });
            }
        });
    }
};
