require('../models/location/countries');
var Countries = mongoose.model('countries');
var consts = require('../const/const');
var locationBL = require('../businesslogic/locationsBL');

var logger = log4js.getLogger('locations.js');

module.exports = {
    //Upload country
    uploadCountries: function (req, res) {
        Countries.count({}, function (err, result) {
            if (err) {
                res.send({ status: false, message: "uploadCountries: error occurred while getting count in countries", err });
            }
            else {
                if (result != undefined && result > 0) {
                    logger.debug("uploadCountries: record present in countries collection");
                    res.send({ status: true, message: consts.SUCCESS, count: result });
                } else {
                    logger.debug("uploadCountries: data uploading is started");
                    locationBL.uploadCountries(res);
                }
            }
        });
    },

    //Countries list
    countriesList: function (req, res) {
        Countries.find({}, function (err, result) {
            if (err) {
                res.send({ status: false, message: "countriesList: error occurred while getting record in countries", err });
            }
            else {                
                logger.debug("countriesList: record found");
                res.send({ status: true, message: consts.SUCCESS, count: result.length, result });                
            }
        });
    },

    //Countries basic info list
    countriesBaicInfo: function (req, res) {
        Countries.find({}, {countryName : 1, iso2: 1},function (err, result) {
            if (err) {
                res.send({ status: false, message: "countriesBaicInfo: error occurred while getting record in countries", err });
            }
            else {                
                logger.debug("countriesBaicInfo: record found");
                res.send({ status: true, message: consts.SUCCESS, count: result.length, result });                
            }
        });
    },

    //Location finder
    locationFinder: function (req, res) {
        locationBL.locationFinder(req, res);
    }
};