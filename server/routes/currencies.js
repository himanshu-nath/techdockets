// var async = require('async');
require('../models/currencies');
require('../models/currencyrates');
require('../models/currencyhistorical');
var Currencies = mongoose.model('currencie');
var CurrencyRates = mongoose.model('currencyrate');
var CurrencyHistories = mongoose.model('currencyhistories');
var consts = require('../const/const');
var currencieBL = require('../businesslogic/currenciesBL');
let uuidv4 = require('uuid/v4');

var logger = log4js.getLogger('currencies.js');

module.exports = {
    //Get currencies list
    getCurrenciesCodeList: function (req, res) {
        Currencies.find({}, function (err, result) {
            if (err) {
                res.send({ status: false, message: "error occurred", err });
            }
            else {
                if (result.length > 0) {
                    logger.debug("Calling  currencies : getCurrenciesList method data found in DB");
                    res.send({ status: true, message: consts.SUCCESS, result });
                } else {
                    logger.debug("Calling  currencies : getCurrenciesList method data is fetching from apilayer.net");
                    currencieBL.fetchCurrencyCodeList(res)
                }
            }
        });        
    },

    //Cron job for real-time exchange rates
    getRealTimeExchangeRates: function () {
        CurrencyRates.count({}, function (err, result) {
            if (err) {
                res.send({ status: false, message: "error occurred", err });
            }
            else {
                if (result > 0) {
                    logger.debug("Calling  currencies : getRealTimeExchangeRates method data found in DB");
                    currencieBL.updateExchangeRates();
                } else {
                    logger.debug("Calling  currencies : getRealTimeExchangeRates method data is fetching from apilayer.net");
                    currencieBL.fetchExchangeRates();
                }
            }
        });
    },

    //Cron job for adding historical rates
    addHistoricalRates: function () {
        CurrencyHistories.aggregate([
            {
                $project:
                {
                    date: {$dayOfMonth: "$date"},
                    month: {$month: "$date"},
                    year: {$year: "$date"}
                }
            }, {
                $match: {date: new Date().getDate(), month: new Date().getMonth() + 1, year: new Date().getFullYear()}
            }
        ], function (err, result) {
            if(err)
                logger.debug("error in addHistoricalRates: "+err);
            else{
                if(result.length > 0) {
                    logger.debug("Record for current date: "+ new Date().toDateString() + " already exists");
                } else {
                    logger.debug("Calling  currencies : addHistoricalRates method for adding date: "+ new Date() +" real-time exchange rates");
                    currencieBL.addingHistoricalRates();
                }
            }
        });
    },

    //Get historical rates by date
    getHistoricalRate: function (req, res) {
        let searchDate = new Date(Number(req.params.date));
        CurrencyHistories.aggregate([
            {
                $project:
                {
                    "_id": 1, "date": 1, "currencies": 1,
                    day: {$dayOfMonth: "$date"},
                    month: {$month: "$date"},
                    year: {$year: "$date"}
                }
            }, {
                $match: {day: searchDate.getDate(), month: searchDate.getMonth() + 1, year: searchDate.getFullYear()}
            }
        ], function (err, result) {
            if(err)
                logger.debug("error in addHistoricalRates: "+err);
            else{
                if(result.length > 0) {
                    logger.debug("Record found in DB for getHistoricalRate method");
                    res.send({ status: true, message: consts.SUCCESS, result });
                } else {
                    logger.debug("Calling  currencies : addHistoricalRates method for adding date: "+ searchDate +" real-time exchange rates");
                    currencieBL.fetchHistoricalRates(searchDate, res);
                }
            }
        });
    },

    //Currency convert    
    currencyConvert: function (req, res) {
         CurrencyRates.find({code: { "$in": [ req.params.from, req.params.to ] } }, {_id: 0, rate: 1, code: 1}, function (err, result) {
            if(err)
                logger.debug("error in finding currency value: "+err);
            else{
                if(result.length > 0) {
                    let convert;
                    let fromRate;
                    let toRate;
                    for(var i in result) {
                        if(req.params.from == result[i].code)
                            fromRate = result[i].rate
                        else if(req.params.to == result[i].code)
                            toRate = result[i].rate
                    }
                    convert = ((toRate/fromRate)*req.params.amount).toFixed(3);
                    logger.debug("Calling  currencies : currencyConvert record found and converted successfully");
                    res.send({ status: true, message: consts.SUCCESS, convert, from: req.params.from, to: req.params.to });
                } else {
                    logger.debug("Calling  currencies : currencyConvert no record found");
                    
                }
            }
        });
    },

    //Get all currency list
    getAllCurrenciesList: function (req, res) {
         CurrencyRates.aggregate([
             { $lookup:
                {
                    from: "currencies",
                    localField: "code",
                    foreignField: "code",
                    as: "currencies_docs"
                }
            },
            { $unwind: 
                {
                    "path": "$currencies_docs",
                    "preserveNullAndEmptyArrays": true
                } 
            },
            { $project:
                {
                    _id : 1, currencies_docs: 1, rate: 1, source: 1, date: 1,
                    currencies_docs: {
                        name: 1,
                        code: 1
                    }
                }
            }
        ], function (err, result) {
            if(err)
                logger.debug("error in addHistoricalRates: "+err);
            else{
                if(result.length > 0) {
                    res.send({ status: true, message: consts.SUCCESS, result });
                } else {
                }
            }
        });
    },

    //Currency by code    
    getCurrencyByCode: function (req, res) {
         CurrencyRates.findOne({code: req.params.code }, {_id: 0, rate: 1, code: 1, date: 1}, function (err, result) {
            if(err)
                logger.debug("error in finding currency value by code: "+err);
            else{
                if(result != undefined) {
                    logger.debug("Calling  currencies : getCurrencyByCode record found");
                    res.send({ status: true, message: consts.SUCCESS, result });
                } else {
                    logger.debug("Calling  currencies : getCurrencyByCode no record found");                    
                }
            }
        });
    },
};
