var Schema   = mongoose.Schema;

var CurrencyHistoricalSchema = new Schema({
    source : String,
    date : {type: Date},
    currencies: [{code: String, rate: Number}],
    cot: {type: Date, default: Date.now},
    mot: {type: Date, default: Date.now}
});
mongoose.model('currencyhistories', CurrencyHistoricalSchema);
