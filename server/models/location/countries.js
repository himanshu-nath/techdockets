var Schema   = mongoose.Schema;

var CountriesSchema = new Schema({
    countryName : String,
    iso2 : String,
    iso3 : String,
    topLevelDomain : String,
    fips : String,
    isoNumeric : Number,
    geoNameId : Number,
    e164 : Number,
    phoneCode : String,
    continent : String,
    capital : String,
    timeZone : String,
    currency : String,
    languageCodes : String,
    languages : String,
    areaKM2 : Number,
    internetHosts : Number,
    internetUsers : Number,
    mobile : Number,
    landline : Number,
    gdp : Number,
    status : Boolean,
    cot: {type: Date, default: Date.now},
    mot: {type: Date, default: Date.now}
});
mongoose.model('countries', CountriesSchema);