var Schema   = mongoose.Schema;

var OilSchema = new Schema({
    state : String,
    state_code : { type: String, get: toLower },
    records: [{date: Date, price: Number, company: String, city: String, oil_type: String}],
    status : Boolean,
    cot: {type: Date, default: Date.now},
    mot: {type: Date, default: Date.now}
});

//setter
function toLower (value) {
  return value.toLowerCase();
}

mongoose.model('oils', OilSchema);