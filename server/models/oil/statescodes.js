var Schema   = mongoose.Schema;

var StateCodeSchema = new Schema({
    name : String,
    code : String,
    status : Boolean,
    cot: {type: Date, default: Date.now},
    mot: {type: Date, default: Date.now}
});
mongoose.model('statecodes', StateCodeSchema);