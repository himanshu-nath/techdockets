var Schema   = mongoose.Schema;

var LanguagesSchema = new Schema({
    name : String,
    code : String,
    status : Boolean,
    cot: {type: Date, default: Date.now},
    mot: {type: Date, default: Date.now}
});
mongoose.model('languages', LanguagesSchema);