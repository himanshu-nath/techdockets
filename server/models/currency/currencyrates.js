var Schema   = mongoose.Schema;

var CurrencyRatesSchema = new Schema({
    code : String,
    rate : Number,
    source : String,
    date : {type: Date, default: Date.now},
    cot: {type: Date, default: Date.now},
    mot: {type: Date, default: Date.now}
});
mongoose.model('currencyrate', CurrencyRatesSchema);


// // Getter
// ItemSchema.path('price').get(function(num) {
//   return (num / 100).toFixed(2);
// });

// // Setter
// ItemSchema.path('price').set(function(num) {
//   return num * 100;
// });