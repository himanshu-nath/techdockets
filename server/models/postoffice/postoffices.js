var Schema   = mongoose.Schema;
mongoosePaginate = require('mongoose-paginate')

var PostofficeSSchema = new Schema({
    office_name : String,
    pincode : String,
    office_type : { type: String, get: toLower },
    delivery_status : String,
    division_name : String,
    region_name : String,
    circle_name : String,
    taluk : String,
    district_name : String,
    state_name : { type: String, set: toUpper } ,
    telephone : String,
    related_suboffice : String,
    related_headoffice : String,
    longitude : { type: String, set: checkNA },
    latitude : { type: String, set: checkNA },
    status : Boolean,
    cot: {type: Date, default: Date.now},
    mot: {type: Date, default: Date.now}
});

PostofficeSSchema.plugin(mongoosePaginate);

//setter
function toUpper (value) {
  return value.toUpperCase();
}

//setter
function checkNA (value) {
  if(value == 'NA')
        return value = 0;
    return value;
}

//getter
function toLower (value) {
    return value.toLowerCase();
}

mongoose.model('postoffices', PostofficeSSchema);