module.exports = function(grunt) {
    grunt.initConfig({
        config: {
            src: '',
            build: 'build'
        },
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            build: {
                src: ['build']
            },
            grunt_bower: {
                src: ['./assets/libs']
            },
            tmp: {
                src: ['./.tmp']
            },
            prod: {
                src: ['./index-prod.html']
            }
        },copy: {
            main: {
                expand: true,
                cwd: './assets/vendor/',
                src: '**',
                dest: 'build/vendor/'
            },
            gif: {
                expand: true,
                cwd: './assets/gif/',
                src: '**',
                dest: 'build/gif/'
            },
            css: {
                expand: true,
                cwd: './assets/css/',
                src: '**',
                dest: 'build/css/'
            },
            font: {
                expand: true,
                cwd: './assets/fonts/',
                src: '**',
                dest: 'build/fonts/'
            },
            js: {
                expand: true,
                cwd: './assets/js/',
                src: '**',
                dest: 'build/js/'
            },
            images: {
                expand: true,
                cwd: './assets/images/',
                src: '**',
                dest: 'build/images/'               
            },
            indexCopy :{
                expand: true, 
                cwd: './',
                src: ['index.html'],
                dest: './',
                rename: function(dest, src) {
                    return dest + 'index-prod.html';
                }
            },
            prodCopy:{
                expand: true,
                cwd: './',
                src:['*-prod.html'],
                rename: function() {
                    return './index.html';
                }
            }
        },
        uglify: {
            generated: {
                options: {
                    sourceMap: true
                }
            }
        },
        bower: {
            install: {
                options: {
                    targetDir: './assets/libs/'
                }
            }
        },
        useminPrepare: {
            html: ['index.html'],
            options: {
                dest: './'
            }
        },
        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 8
            },
            js: {
                src: ['build/js/*.js', 'build/css/*.css']
            }
        },
        usemin: {
            html: ['index.html'],
            options: {
                blockReplacements: {
                    css: function( block ) {
                        return '<link rel="stylesheet" href="' + block.dest + '"/>';
                    },
                    js: function( block ) {
                        return '<script src="' + block.dest + '"></script>';
                    }
                }
            }
        }
    });

   grunt.log.write('Grunt file loaded\n');

   grunt.loadNpmTasks('grunt-contrib-clean');
   grunt.loadNpmTasks('grunt-contrib-copy');
   grunt.loadNpmTasks('grunt-contrib-concat');
   grunt.loadNpmTasks('grunt-contrib-cssmin');
   grunt.loadNpmTasks('grunt-contrib-uglify');
   grunt.loadNpmTasks('grunt-filerev');
   grunt.loadNpmTasks('grunt-usemin');
   grunt.loadNpmTasks('grunt-bower-task');
   grunt.loadNpmTasks('grunt-npm-install');

    // Prod task(s).
    grunt.registerTask('prod', [
        'clean:build',
        'copy:main',
        'copy:gif',
        'copy:css',
        'copy:js',
        'copy:font',
        'copy:images',
        'copy:indexCopy',
        'useminPrepare',
        'concat:generated',
        'cssmin:generated',
        'uglify:generated',
        'filerev',
        'usemin'
        //,'clean:grunt_bower'
    ]);

    // Localhost task(s).
    grunt.registerTask('local', [
        'clean:build',
        'copy:prodCopy',
        'clean:prod',
        'clean:tmp'
        //,'bower:install' 
    ]);
};