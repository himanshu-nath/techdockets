angular.module('techDocket')
    .factory('LanguageTranslateService', ['appConfig', '$resource', 
    function (appConfig, $resource) {
        return {
            resource: function () {
                return $resource(appConfig.serviceUrl + '/lang', {
                    id: '@_id'
                }, {
                        languageList: {
                            method: 'GET',
                            url: appConfig.serviceUrl + '/api/translate/languages'
                        },
                        languageDetect: {
                            method: 'GET',
                            url: appConfig.serviceUrl + '/api/translate/detect?text=:text'
                        },
                        languageConvert: {
                            method: 'POST',
                            url: appConfig.serviceUrl + '/api/translate/convert'
                        }
                    })
            }
        }
    }])