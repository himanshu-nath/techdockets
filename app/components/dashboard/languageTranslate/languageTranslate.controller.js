angular.module('techDocket')
.controller('LanguageTranslateController', ['$state', 'appConfig','LanguageTranslateService','$log', '$rootScope',
 function($state, appConfig, LanguageTranslateService, $log, $rootScope){
     
    var vm = this;

    init();
    function init(){
        getLanguageList();
    }

    function getLanguageList(){
        $rootScope.viewSpinner = true;
        LanguageTranslateService.resource(appConfig).languageList({},
        function(response){
            $rootScope.viewSpinner = false;
            vm.languageList = response.result;
        }, function(error){
            $rootScope.viewSpinner = false;
            console.log("getLanguageList: error occurred due to: "+error);
            swal({
                position: 'bottom-end',
                type: 'error',
                title: 'Error while featching language list',
                showConfirmButton: false,
                timer: 2500
            })
        })
    }

    vm.convertText = function() {
        $rootScope.viewSpinner = true;
        LanguageTranslateService.resource(appConfig).languageConvert({
            from: typeof vm.fromlanguage == "object" ? vm.fromlanguage.code : vm.fromlanguage,
            to: vm.tolanguage,
            text: vm.inputText
        },
        function(response){
            $rootScope.viewSpinner = false;
            if(response.status == true && response.message == "SUCCESS") {
                vm.outputText = response.text;
            } else if(response.status == true && response.message == "FAILED"){
                swal({
                    position: 'bottom-end',
                    type: 'warning',
                    title: 'Failed to convert your text',
                    text: 'Field cannot be empty!',
                    showConfirmButton: false,
                    timer: 2500
                  })
            } else {
                swal({
                    position: 'bottom-end',
                    type: 'error',
                    title: 'Failed to convert your text',
                    showConfirmButton: false,
                    timer: 2500
                  })
            } 
        }, function(error){
            $rootScope.viewSpinner = false;
            console.log("convertText: error occurred due to: "+error);
            swal({
                position: 'bottom-end',
                type: 'error',
                title: 'Error while converting text',
                showConfirmButton: false,
                timer: 2500
            })
        })
    }

    vm.textLanguageDetect = function() {
        $rootScope.viewSpinner = true;
        LanguageTranslateService.resource(appConfig).languageDetect({
            text: vm.inputText
        },
        function(response){
            $rootScope.viewSpinner = false;
            if(response.status == true) {
                vm.fromlanguage = response.result;
            } else {
                swal({
                    position: 'bottom-end',
                    type: 'error',
                    title: 'Failed detect language from text',
                    showConfirmButton: false,
                    timer: 2500
                  })
            }
        }, function(error){
            $rootScope.viewSpinner = false;
            console.log("convertText: error occurred due to: "+error);
            swal({
                position: 'bottom-end',
                type: 'error',
                title: 'Error while language detection',
                showConfirmButton: false,
                timer: 2500
            })
        })
    }
    
}])