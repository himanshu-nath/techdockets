angular.module('techDocket')
    .controller('SpeechConverterController', ['$state', 'appConfig', 'SpeechConverterService', 
        '$log', 'constUrls', 'Upload', '$scope', '$rootScope',
        function ($state, appConfig, SpeechConverterService, $log, constUrls, Upload, $scope, 
            $rootScope) {

            var vm = this;
            init();
            function init() {
                speechLanguageList();
            }

            function speechLanguageList() {
                $rootScope.viewSpinner = true;
                SpeechConverterService.resource(appConfig).speechLanguageList({},
                    function (response) {
                        vm.speechList = response.languages;
                        $rootScope.viewSpinner = false;
                    }, function (error) {
                        $rootScope.viewSpinner = false;
                        console.log("speechLanguageList: error occurred due to: " + error);
                        swal({
                            position: 'bottom-end',
                            type: 'error',
                            title: 'Error while featching speech list',
                            showConfirmButton: false,
                            timer: 2500
                        })
                    })
            }

            vm.textToSpeech = function () {
                $rootScope.viewSpinner = true;
                vm.textToSpeechUrl = appConfig.serviceUrl + constUrls.textToSpeech + '?text=' + vm.inputText; + '&voice=' + vm.toSpeech;
                console.log(vm.textToSpeechUrl);
                angular.element("#wav_src")[0].src =vm.textToSpeechUrl;
                angular.element("#convertTextToSpeech")[0].load();
                $rootScope.viewSpinner = false;

            }

            vm.speechToText = function () {
                $rootScope.viewSpinner = true;
                if (vm.audioFile != null && vm.audioFile.type.match(/audio/g)) {
                    Upload.upload({
                        url: appConfig.serviceUrl + constUrls.speechToText,
                        data: {audioFile: vm.audioFile}
                    }).then(function (resp) {
                        vm.outputText = resp.data.transcript;
                        $rootScope.viewSpinner = false;
                        console.log(resp);                        
                    }, function (error) {
                        $rootScope.viewSpinner = false;
                        swal({
                            position: 'top-end',
                            type: 'error',
                            title: 'Failed to convert speech to audio',
                            showConfirmButton: false,
                            timer: 2500
                          })
                    }, function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    });
                  } else if (vm.audioFile != null && !vm.audioFile.type.match(/audio/g)){
                    swal({
                        title: 'File Type Error',
                        html: $('<div>')
                          .addClass('some-class')
                          .text('Please select audio file only.'),
                        animation: false,
                        customClass: 'animated shake'
                      })
                  } else {
                    swal({
                        title: "File can't be empty",
                        html: $('<div>')
                          .addClass('some-class')
                          .text('Please select a file'),
                        animation: false,
                        customClass: 'animated shake'
                      })
                }
                $rootScope.viewSpinner = false;
            }

        }])