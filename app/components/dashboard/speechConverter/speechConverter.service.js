angular.module('techDocket')
    .factory('SpeechConverterService', ['appConfig', '$resource', 
    function (appConfig, $resource) {
        return {
            resource: function () {
                return $resource(appConfig.serviceUrl + '/lang', {
                    id: '@_id'
                }, {
                        speechLanguageList: {
                            method: 'GET',
                            url: appConfig.serviceUrl + '/api/speech/languages'
                        },
                        textToSpeech: {
                            method: 'POST',
                            url: appConfig.serviceUrl + '/api/textToSpeech'
                        },
                        speechToText: {
                            method: 'POST',
                            url: appConfig.serviceUrl + '/api/speechToText'
                        }
                    })
            }
        }
    }])