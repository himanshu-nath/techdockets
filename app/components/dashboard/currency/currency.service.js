angular.module('techDocket')
.factory('currencyService', ['appConfig', '$resource', 
function(appConfig, $resource){
    return {
        resource: function(){
            return $resource(appConfig.serviceUrl + '', {
                id : '@_id'
            }, {
                getCurrency : {
                    method: 'GET',
                    url: appConfig.serviceUrl + ''
                }
            });
        }
    }
}])