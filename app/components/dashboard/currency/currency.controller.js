angular.module('techDocket')
.controller('currencyController', ['$state', '$log',
function($state, $log){
    var vm = this;

    swal({
        title: 'Stay Tuned',
        text: 'Page Under Construction',
        imageUrl: 'assets/gif/coming_soon1.gif',
        imageWidth: 400,
        imageHeight: 200,
        imageAlt: 'Coming Soon',
        animation: false,
        timer: 10500,
        customClass: 'animated tada',
        confirmButtonText: 'Back'
      }).then(function(result) {
        $state.go('dashboard.home');
      })
}])