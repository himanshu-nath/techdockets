angular.module('techDocket')
  .controller('homeController', ['$state', '$rootScope', 'HomeService', 'toastAlert', 'appConfig', '$scope',
  function ($state, $rootScope, HomeService, toastAlert, appConfig, $scope) {
    var vm = this;

    master = {};
    var toast = toastAlert;

    vm.sendEmail = function () {
      $rootScope.viewSpinner = true;
      HomeService.resource(appConfig).sendMail(vm.mail, {},
        function (response) {
          $rootScope.viewSpinner = false;
          if (response.status) {
            toast({
              type: 'success',
              title: 'Email send successfully'
            });
            vm.mail = angular.copy(master);
            $scope.emailForm.$setUntouched();
            $scope.emailForm.$setPristine();
          } else {
            toast({
              type: 'error',
              title: 'Email sending failed'
            });
          }
        }, function (error) {
          $rootScope.viewSpinner = false;
          console.log("speechLanguageList: error occurred due to: " + error);
          toast({
            type: 'error',
            title: 'Email sending failed'
          });
        })
    }

  }
  ]);
