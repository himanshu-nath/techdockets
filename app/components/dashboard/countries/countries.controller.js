angular.module('techDocket')
.controller('CountriesController', ['$state', 'CountriesService', 'appConfig', '$log', '$rootScope',
function($state, CountriesService, appConfig, $log, $rootScope ){
    
    var vm = this;
    init();
    function init() {
        langList();
    }

    function langList(){
        $rootScope.viewSpinner = true;
        CountriesService.resource(appConfig).countryList({},
        function(response){
            console.log(response);
            $rootScope.viewSpinner = false;
            if(!response.status) {
                swal({
                    position: 'bottom-end',
                    type: 'error',
                    title: 'Error while featching countires list',
                    showConfirmButton: false,
                    timer: 2500
                })
            } else {
                vm.countrylist = response.result;
            }
        }, function(error){
            $rootScope.viewSpinner = true;
            console.log("error occurred");
            swal({
                position: 'bottom-end',
                type: 'error',
                title: 'Error while featching languages list',
                showConfirmButton: false,
                timer: 2500
            })
        })
    }

}])