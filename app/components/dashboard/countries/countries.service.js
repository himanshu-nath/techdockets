angular.module('techDocket')
    .factory('CountriesService', ['appConfig', '$resource',
        function (appConfig, $resource) {
            return {
                resource: function () {
                    return $resource(appConfig.serviceUrl + '/langList', {
                        id: '@_id'
                    }, {
                        countryList: {
                                method: 'GET',
                                url: appConfig.serviceUrl + '/api/location/countriesList'
                            }
                        });
                }
            }
        }])