angular.module('techDocket')
.factory('postOfficeService', ['appConfig', '$resource',
function(appConfig, $resource) {
    return {
        resource: function(){
            return $resource(appConfig.serviceUrl + '/postOffice',{
                id: '@_id'
            }, {
                getPOList : {
                    method: 'GET',
                    url : appConfig.serviceUrl + '/api/postoffice/list?page=:currentPageNumber&limit=:pageOffset',
                }             
            });
        }
    }
}])