angular.module('techDocket')
    .controller('NewsController', ['$state', 'appConfig', 'NewsService',
        '$log', 'constUrls', 'Upload', '$scope', '$rootScope', 'NEWS_VALUES',
        function ($state, appConfig, NewsService, $log, constUrls, Upload, $scope, $rootScope, NEWS_VALUES) {

            var vm = this;
            vm.categories = NEWS_VALUES.CATEGORIES;
            vm.countries = NEWS_VALUES.COUNTRY;
            vm.languages = NEWS_VALUES.LANGUAGE;
            vm.sortBys = NEWS_VALUES.SORT_BY;

            init();
            function init() {
                getSourceList();
            }

            function getSourceList() {
                $rootScope.viewSpinner = true;
                NewsService.resource(appConfig).getSourceList({},
                    function (response) {
                        console.log(response);
                        if(!response.status && response.message == "ERROR") {
                            swal({
                                title: response.error.code,
                                html: $('<div>')
                                  .addClass('some-class')
                                  .text('Failed to fetch source list'),
                                animation: false,
                                customClass: 'animated shake'
                              })
                        } else {                            
                            vm.sourceList = response.results.sources;                      
                        }
                        $rootScope.viewSpinner = false;
                    }, function (error) {
                        console.log("speechLanguageList: error occurred due to: " + error);
                        $rootScope.viewSpinner = false;
                        swal({
                            position: 'bottom-end',
                            type: 'error',
                            title: 'Error while featching news channel list',
                            showConfirmButton: false,
                            timer: 2500
                        })
                    })
            }

            vm.headlineSearch = function() {
                $rootScope.viewSpinner = true;
                NewsService.resource(appConfig).topHeadlines({
                    countryCode: vm.countryCode,
                    category: vm.categoryCode,
                    search: vm.searchHText,
                    source: vm.sourceCode
                },
                    function (response) {
                        $rootScope.viewSpinner = false;
                        if(!response.status) {
                            swal({
                                position: 'bottom-end',
                                type: 'error',
                                title: 'Error while featching news haedline',
                                showConfirmButton: false,
                                timer: 2500
                              })
                        } else {
                            vm.headLineList = response.results.articles;
                        }
                    }, function (error) {
                        console.log("speechLanguageList: error occurred due to: " + error);
                        $rootScope.viewSpinner = false;
                        swal({
                            position: 'bottom-end',
                            type: 'error',
                            title: 'Error while featching headlines',
                            showConfirmButton: false,
                            timer: 2500
                        })
                    })
            }

            vm.headlineClear = function() {
                vm.searchHText = '';
                if(angular.isDefined(vm.sourceCode) || angular.isDefined(vm.countryCode) || angular.isDefined(vm.categoryCode)){
                    delete vm.sourceCode;
                    delete vm.countryCode;
                    delete vm.categoryCode;
                }
            }

            vm.clearHeadline = function() {
                vm.headLineList = null;
            }

            vm.everythingSearch = function() {
                vm.pageNumberE = vm.pageNumberE != undefined ? vm.pageNumberE : 1;
                vm.pageSizeE = vm.pageSizeE != undefined ? vm.pageSizeE : 40;
                $rootScope.viewSpinner = true;
                NewsService.resource(appConfig).everything({
                    language: vm.languageCode,
                    sortBy: vm.sortByCode,
                    search: vm.searchEText,
                    source: vm.sourceCodeE,
                    pageNumber: vm.pageNumberE,
                    pageOffset: vm.pageSizeE
                },
                    function (response) {
                        $rootScope.viewSpinner = false;
                        if(!response.status) {
                            swal({
                                position: 'bottom-end',
                                type: 'error',
                                title: 'Error while featching everthing news',
                                showConfirmButton: false,
                                timer: 2500
                              })
                        } else {
                            vm.everythingList = response.results.articles;
                        }
                    }, function (error) {
                        console.log("speechLanguageList: error occurred due to: " + error);
                        $rootScope.viewSpinner = false;
                        swal({
                            position: 'bottom-end',
                            type: 'error',
                            title: 'Error while featching news list',
                            showConfirmButton: false,
                            timer: 2500
                        })
                    })
            }

            vm.everythingClear = function() {
                vm.searchEText = '';
                vm.pageNumberE = '';
                vm.pageSizeE = '';
                if(angular.isDefined(vm.sourceCodeE) || angular.isDefined(vm.languageCode) || angular.isDefined(vm.sortByCode)) {
                    delete vm.sourceCodeE;
                    delete vm.languageCode;
                    delete vm.sortByCode;
                }
            }

            vm.clearEverythingList = function() {
                vm.everythingList = null;
            }
        }]
    )