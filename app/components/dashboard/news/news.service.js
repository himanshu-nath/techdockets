angular.module('techDocket')
    .factory('NewsService', ['appConfig', '$resource', 
    function (appConfig, $resource) {
        return {
            resource: function () {
                return $resource(appConfig.serviceUrl + '/lang', {
                    id: '@_id'
                }, {
                        topHeadlines: {
                            method: 'GET',
                            url: appConfig.serviceUrl + '/api/headlines?countryCode=:countryCode&category=:category&search=:search&source=:source'
                        },
                        getSourceList: {
                            method: 'GET',
                            url: appConfig.serviceUrl + '/api/sources?countryCode=&language='
                        },
                        everything: {
                            method: 'GET',
                            url: appConfig.serviceUrl + '/api/everything?search=:search&source=:source&language=:language&sortBy=:sortBy&pageNumber=:pageNumber&pageOffset=:pageOffset'
                        }
                    })
            }
        }
    }])