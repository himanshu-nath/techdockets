angular.module('techDocket')
.factory('LocationService', ['appConfig', '$resource', 
function(appConfig, $resource){
    return {
        resource: function(){
            return $resource(appConfig.serviceUrl + '', {
                id: '@_id'
            }, {
                getOilPrice : {
                    method: 'GET',
                    url: appConfig.serviceUrl + ''
                }
            })
        }
    }
}])