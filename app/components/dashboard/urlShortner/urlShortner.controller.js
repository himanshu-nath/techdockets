angular.module('techDocket')
	.controller('UrlShortnerController', ['$state', 'appConfig', 'UrlShortnerService', '$log','$rootScope',
		function ($state, appConfig, UrlShortnerService, $log, $rootScope) {
			var vm = this;

			vm.urlShortner = function () {
				$rootScope.viewSpinner = true;
				UrlShortnerService.resource(appConfig).urlShortner({
					"url": vm.shortUrlInput
				},
					function (response) {
						console.log(response)
						vm.shortUrlOutput = response.results.id;
						$rootScope.viewSpinner = false;
					}, function (error) {
						$rootScope.viewSpinner = false;
						console.log("Error occurred");
						swal({
							position: 'bottom-end',
							type: 'error',
							title: 'Error while url shortner',
							showConfirmButton: false,
							timer: 2500
						})
					});
			}

			vm.urlExpander = function() {
				$rootScope.viewSpinner = true;
				UrlShortnerService.resource(appConfig).urlExpander({
					"url": vm.expandUrlInput
				}, function (response) {
					console.log(response);
					vm.expandUrlOutput = response.results.longUrl;
					$rootScope.viewSpinner = false;
					console.log(vm.expandUrlOutput);
				}, function (error) {
					$rootScope.viewSpinner = false;
					console.log("Error occurred");
					swal({
						position: 'bottom-end',
						type: 'error',
						title: 'Error while url expander',
						showConfirmButton: false,
						timer: 2500
					})
				});
			}

		}])