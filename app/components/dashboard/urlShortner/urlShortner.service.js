angular.module('techDocket')
.factory('UrlShortnerService', ['appConfig', '$resource', function(appConfig, $resource){
        return {
         resource: function(){
             return $resource(appConfig.serviceUrl + '/url',{
                 id: '@_id'
             }, {
                urlShortner : {
                    method: 'POST',
                    url: appConfig.serviceUrl + '/api/url/shortener'

                },
                urlExpander : {
                    method : 'POST',
                    url: appConfig.serviceUrl + '/api/url/expand'

                }
             })
         }   
        }
}])