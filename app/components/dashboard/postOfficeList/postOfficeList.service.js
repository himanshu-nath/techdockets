angular.module('techDocket')
    .factory('PostOfficeListService', ['appConfig', '$resource',
        function (appConfig, $resource) {
            return {
                resource: function () {
                    return $resource(appConfig.serviceUrl + '/postOffice', {
                        id: '@_id'
                    }, {
                            getPOList: {
                                method: 'GET',
                                url: appConfig.serviceUrl + '/api/postoffice/list?page=:currentPageNumber&limit=:pageOffset',
                            },

                             getPOListByPin: {
                                method: 'GET',
                                url: appConfig.serviceUrl + '/api/postoffice/pincode/:pincode/:currentPageNumber/:pageOffset',
                            },

                            getStatePOList: {
                                method: 'GET',
                                url: appConfig.serviceUrl + '/api/postoffice/state/bihar/1/100'
                            },

                            getDistrictPOList: {
                                method: 'GET',
                                url: appConfig.serviceUrl + '/api/postoffice/district/gaya/1/100'
                            },

                            getCityPOList : {
                                method: 'GET',
                                url: appConfig.serviceUrl + '/api/postoffice/city/dehri/1/100'
                            }
                        });
                }
            }
        }])