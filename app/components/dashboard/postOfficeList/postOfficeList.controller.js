angular.module('techDocket')
.controller('PostOfficeListController', ['$state', '$stateParams', '$scope', '$rootScope',  'PostOfficeListService', '$log', 'appConfig',
function($state, $stateParams, $scope, $rootScope, PostOfficeListService, $log, appConfig){
    var vm = this;

    vm.getPOList = getPOList;
    vm.currentPage = 1;
    vm.dataPerPage = 12;

    init();
    function init(){
        console.log($stateParams.flag);
        console.log($stateParams.search);

        switch($stateParams.flag) {
            case "pincode":
                getPOListByPin(vm.currentPage, vm.dataPerPage); 
                break;
            default:
                getPOList(vm.currentPage, vm.dataPerPage);
        }
        
        //loadPages()      
    }

    function loadPages(){
        getPOList(vm.currentPage, vm.dataPerPage);		
    }
    
    vm.pageChanged = function(page) {
        vm.currentPage = page;
        getPOList(vm.currentPage, vm.dataPerPage);
    }
	
    function getPOList(currentPageNumber, pageOffset){
        $rootScope.viewSpinner = true;
        console.log("inside getPOList");
        PostOfficeListService.resource(appConfig).getPOList({
            currentPageNumber: currentPageNumber,
			pageOffset: pageOffset
        },
        function(response){
            $rootScope.viewSpinner = false;
            if(response.status && response.message.toUpperCase() == "SUCCESS") {
                vm.postOfficeList = response.result.docs;
                vm.paging = {
                        total : response.result.total,
                        current: vm.currentPage,
                        size: vm.dataPerPage
                }
            }            
        }, function(error){
            console.log("error occurred");
            swal({
                position: 'top-end',
                type: 'error',
                title: 'Failed to get post office list',
                showConfirmButton: false,
                timer: 2500
              })
        })
    }

    function getPOListByPin(currentPageNumber, pageOffset){
        console.log("inside getPOListByPin");
        PostOfficeListService.resource(appConfig).getPOListByPin({
            pincode: $stateParams.search,
            currentPageNumber: vm.currentPage,
			pageOffset: vm.dataPerPage
        }, function(response){
            if(response.status && response.message.toUpperCase() == "SUCCESS"){
                vm.postOfficeList = response.result.docs;
                vm.paging = {
                    total : response.result.total,
                    current: vm.currentPage,
                    size: vm.dataPerPage
                }                
            }
        }, function(error){
            console.log("error occurred");
            swal({
                position: 'top-end',
                type: 'error',
                title: 'Failed to get post office list',
                showConfirmButton: false,
                timer: 2500
              })
        })
    }

    function getStatePOList(){
        PostOfficeListService.resource(appConfig).getStatePOList({

        }, function(response){
            if(response.status && response.message.toUpperCase() == "SUCCESS"){
                vm.postOfficeList = response.result.docs;
                console.log(vm.postOfficeList);
            }
        }, function(error){
            console.log("error occurred");
        });
    }

    function getDistrictPOList(){
        PostOfficeListService.resource(appConfig).getDistrictPOList({

        }, function(response){
            if(response.status && response.message.toUpperCase() == "SUCCESS"){
                vm.postOfficeList = response.result;
                console.log(vm.postOfficeList);
            }
        }, function(error){
            console.log("error occurred");
        });
    }

    function getCityPOList(){
        PostOfficeListService.resource(appConfig).getCityPOList({

        }, function(response){
             if(response.status && response.message.toUpperCase() == "SUCCESS"){
                vm.postOfficeList = response.result;
                console.log(vm.postOfficeList);
            }
        }, function(error){
            console.log("Error occurred");
        });
    }
}])