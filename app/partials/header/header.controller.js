angular.module('techDocket')
.controller('HeaderController', ['$scope', 'HeaderService', 'localStorageService',
	function($scope, HeaderService, localStorageService) {

	var vm = this;
	
	$('.nav li a').click(function(e) {
		
				$('.nav li.active').removeClass('active');
		
				var $parent = $(this).parent();
				$parent.addClass('active');
				e.preventDefault();
			});

}]);