angular.module('techDocket')
	.factory('HeaderService', ['appConfig', '$resource',
		function (appConfig, $resource) {
			return {
				resource: function () {
					return $resource(appConfig.serviceUrl + '/postOffice', {
						id: '@_id'
					});
				}
			}
		}
	]
)