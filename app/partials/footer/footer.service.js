angular.module('techDocket')
	.factory('FooterService', ['appConfig', '$resource',
		function (appConfig, $resource) {
			return {
				resource: function () {
					return $resource(appConfig.serviceUrl + '/postOffice', {
						id: '@_id'
					});
				}
			}
		}
	]
)