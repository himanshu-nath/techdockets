angular.module('techDocket')
  .config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
      
      //locationProvider
      $stateProvider
        .state('dashboard', {
          url: '/dashboard',
          controller: 'dashboardController',
          controllerAs: 'dCTRL',
          templateUrl: 'app/components/dashboard/dashboard.view.html',
          authenticate: true
        })

        .state('dashboard.home', {
          url: '/home',
          controller: 'homeController',
          controllerAs: 'hCTRL',
          templateUrl: 'app/components/dashboard/home/home.view.html',
          authenticate: true
        })

        .state('dashboard.currency', {
          url: '/currency',
          controller: 'currencyController',
          controllerAs: 'cCTRL',
          templateUrl: 'app/components/dashboard/currency/currency.view.html',
          authenticate: true
        })

        .state('dashboard.postOffice', {
          url: '/postOffice',
          controller: 'postOfficeController',
          controllerAs: 'pOCTRL',
          templateUrl: 'app/components/dashboard/postOffice/postOffice.view.html',
          authenticate: true
        })

        .state('dashboard.postOfficeList', {
          url: '/postOffice/List/:flag/:search',
          controller: 'PostOfficeListController',
          controllerAs: 'POLCtrl',
          templateUrl: 'app/components/dashboard/postOfficeList/postOfficeList.view.html',
          authenticate: true
        })

        .state('listPost', {
          url: '/listPost',
          controller: '',
          controllerAs: '',
          templateUrl: 'app/components/dashboard/listPost/listPost.view.html',
          authenticate: true
        })

        .state('dashboard.oilPrices', {
          url: '/oilPrices',
          controller: 'oilPriceController',
          controllerAs: 'oPCTRL',
          templateUrl: 'app/components/dashboard/oilPrice/oilPrice.view.html',
          authenticate: true
        })

        .state('dashboard.urlShortner', {
          url: '/url/shortner',
          controller: 'UrlShortnerController',
          controllerAs : 'USCtrl',
          templateUrl: 'app/components/dashboard/urlShortner/urlShortner.view.html',
          authenticate: true
        })

        .state('dashboard.countries', {
          url: '/countries',
          controller: 'CountriesController',
          controllerAs: 'CountryCtrl',
          templateUrl: 'app/components/dashboard/countries/countries.view.html',
          authenticate: true
        })

        .state('dashboard.languageTranslate', {
          url: '/language/translate',
          controller: 'LanguageTranslateController',
          controllerAs: 'LTCtrl',
          templateUrl: 'app/components/dashboard/languageTranslate/languageTranslate.view.html',
          authenticate: true
        })

        .state('dashboard.speech', {
          url: '/speech',
          controller: 'SpeechConverterController',
          controllerAs: 'SCCtrl',
          templateUrl: 'app/components/dashboard/speechConverter/speechConverter.view.html',
          authenticate: true
        })

        .state('dashboard.news', {
          url: '/news',
          controller: 'NewsController',
          controllerAs: 'NCtrl',
          templateUrl: 'app/components/dashboard/news/news.view.html',
          authenticate: true
        })

        .state('dashboard.location', {
          url: '/location',
          controller: 'LocationController',
          controllerAs: 'LCtrl',
          templateUrl: 'app/components/dashboard/location/location.view.html',
          authenticate: true
        });

        $urlRouterProvider.otherwise('/dashboard/home');
        // $locationProvider.html5Mode(true);
    }
  ])

  .run(['$rootScope', '$state',
    function ($rootScope, $state) {
      $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {

      });
    }]);