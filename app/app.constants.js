angular.module('techDocket')
.constant('appConfig', {
	'version': 1.0,
	'serviceUrl' : APP.config.serviceUrl 
})
.constant('ignourURLblocking', [

])
.constant('constUrls', {
	'textToSpeech' : '/api/textToSpeech',
	'speechToText' : '/api/speechToText'
})