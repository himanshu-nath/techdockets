angular.module('techDocket', [ 
    'ui.router', 
    'LocalStorageModule', 
    'angular-loading-bar',
    'ngResource',
    'ui.bootstrap',
    'ngAnimate', 
    'ngSanitize',
    'ngFileUpload',
    'bw.paging'
]);