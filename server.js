mongoose = require('mongoose');
var db = require('./config/db');
CronJob = require('cron').CronJob;
log4js = require('log4js');
moment = require('moment');
const nodemailer = require('nodemailer');
const fileUpload = require('express-fileupload');
async = require('async');
request = require('request');
const express = require('express')
const app = express()
bodyParser = require("body-parser");

log4js.configure({
  appenders: [
    { type: 'console' },
    { type: 'file', filename: 'logs/server.log', category: 'server.js' },
    { type: 'file', filename: 'logs/server.log', category: 'currencies.js' },
    { type: 'file', filename: 'logs/server.log', category: 'currencyCron.js' },
    { type: 'file', filename: 'logs/server.log', category: 'currenciesBL.js' },
    { type: 'file', filename: 'logs/server.log', category: 'postoffices.js' },
    { type: 'file', filename: 'logs/server.log', category: 'postofficesBL.js' },
    { type: 'file', filename: 'logs/server.log', category: 'oils.js' },
    { type: 'file', filename: 'logs/server.log', category: 'oilsBL.js' },
    { type: 'file', filename: 'logs/server.log', category: 'oilCron.js' },
    { type: 'file', filename: 'logs/server.log', category: 'speech.js' },
    { type: 'file', filename: 'logs/server.log', category: 'news.js' }
  ]
});

var consts = require('./server/const/const.js')
require('./server/crons/currencyCron.js')
require('./server/crons/postofficeCron.js')
require('./server/crons/oilCron.js')


var logger = log4js.getLogger('server.js');

app.use(fileUpload());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 1000000 }));
app.use(bodyParser.json({ limit: '50mb', parameterLimit: 1000000 }));

var currencies = require('./server/routes/currencies');
var postoffices = require('./server/routes/postoffices');
var oils = require('./server/routes/oils');
var translates = require('./server/routes/translates');
var urls = require('./server/routes/urls');
var locations = require('./server/routes/locations');
var speech = require('./server/routes/speech');
var news = require('./server/routes/news');
var mails = require('./server/routes/mail');

//Currency APIs
app.get('/api/currencies/codelist', currencies.getCurrenciesCodeList);
app.get('/api/currencies/historical/:date', currencies.getHistoricalRate);
app.get('/api/currencies/convert/:from/:to/:amount', currencies.currencyConvert);
app.get('/api/currencies/list', currencies.getAllCurrenciesList);
app.get('/api/currencies/:code', currencies.getCurrencyByCode);

//Postoffice APIs
app.post('/api/postoffice/upload', postoffices.uploadPostoffice);
app.get('/api/postoffice/list', postoffices.getPostofficeList); //for queryString (http://localhost:3030/api/postoffice/list?page=3&limit=100)
app.get('/api/postoffice/pincode/:pincode/:page/:limit', postoffices.getPostofficeByPincode);
app.get('/api/postoffice/state/:state/:page/:limit', postoffices.getPostofficeByState); //for queryParams (http://localhost:3030/api/postoffice/state/BIHAR/1/10)
app.get('/api/postoffice/district/:district/:page/:limit', postoffices.getPostofficeByDistrict);
app.get('/api/postoffice/city/:city/:page/:limit', postoffices.getPostofficeByCity);
app.get('/api/postoffice/office/:office/:page/:limit', postoffices.getPostofficeByOffice);

//Oil APIs
app.get('/api/oil/stateCode', oils.getStateCodes);
app.get('/api/oil/state/:state', oils.getOilInfoByState);
app.get('/api/oil/upload', oils.getRealTimeOilPrice);
app.get('/api/oil/filter', oils.getOilInfoByFiltering); //http://localhost:3030/api/oil/filter?city=Chandigarh&company=Indian&oilType=Petrol&date=2017-11-09
app.get('/api/oil/date', oils.getOilInfoByDate); //http://localhost:3030/api/oil/date?startDate=2017-11-09&endDate=2017-11-11 (For 2 day)

//Language Translate
app.get('/api/translate/languagesUpload', translates.uploadLanguages);
app.get('/api/translate/languages', translates.languageList);
app.get('/api/translate/detect', translates.languageDetect);
app.post('/api/translate/convert', translates.languageConvert);

//Speech
app.get('/api/speech/languages', speech.speechLanguageList);
app.get('/api/textToSpeech', speech.textToSpeech);
app.post('/api/speechToText', speech.speechToText);

//News
app.get('/api/headlines', news.topHeadline);
app.get('/api/everything', news.everything);
app.get('/api/sources', news.getSources);

//URL Shortener
app.post('/api/url/shortener', urls.shortener);
app.post('/api/url/expand', urls.expand);

//Location
app.get('/api/location/countriesUpload', locations.uploadCountries);
app.get('/api/location/countriesList', locations.countriesList);
app.get('/api/location/countries/basicInfo', locations.countriesBaicInfo);
app.post('/api/location/finder', locations.locationFinder);

app.post('/api/mailsend', mails.sendMail);

// app.use('/', express.static(__dirname + '/'));
app.use('/app', express.static(__dirname + '/app'));
app.use('/assets', express.static(__dirname + '/assets'));
app.use('/build', express.static(__dirname + '/build'));
app.use('/.tmp', express.static(__dirname + '/.tmp'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.listen(consts.port, function () {
  logger.debug("server started on port: " + consts.port);
});